"""
This is the file for the actual CNN, contains architecture mostly. 
"""
import random 
import numpy as np

from tensorflow_examples.models.pix2pix import pix2pix
import tensorflow as tf

from callback import DisplayCallback
from constants import *


class UNet:
    def __init__(self, unet_output_channels=OUTPUT_CHANNELS, last_layer_activation='softmax', image_size=IMAGE_SIZE):

        self.encoder = tf.keras.applications.MobileNetV2(input_shape=[image_size, image_size, 3], include_top=False)

        self.build_unet(unet_output_channels, last_layer_activation, image_size)

    def build_upstack(self):
        self.up_stack_layers = [
            pix2pix.upsample(512, 3),  # 4x4 -> 8x8
            pix2pix.upsample(256, 3),  # 8x8 -> 16x16
            pix2pix.upsample(128, 3),  # 16x16 -> 32x32
            pix2pix.upsample(64, 3),  # 32x32 -> 64x64
        ]

    def build_downstack(self):
        down_stack_layer_names = [
            'block_1_expand_relu',  # 64x64
            'block_3_expand_relu',  # 32x32
            'block_6_expand_relu',  # 16x16
            'block_13_expand_relu',  # 8x8
            'block_16_project',  # 4x4
        ]

        layers = [self.encoder.get_layer(name).output for name in down_stack_layer_names]

        self.downstack = tf.keras.Model(inputs=self.encoder.input, outputs=layers)

        self.downstack.trainable = False

    def build_unet(self, output_channels, last_layer_activation, image_shape):

        self.build_upstack()
        self.build_downstack()

        last_layer = tf.keras.layers.Conv2DTranspose(
            output_channels, 3, strides=2,
            padding='same',
            activation=last_layer_activation
        )

        inputs = tf.keras.layers.Input(shape=[image_shape, image_shape, 3])

        x = inputs

        skips = self.downstack(x)
        x = skips[-1]
        skips = reversed(skips[:-1])

        for up, skip in zip(self.up_stack_layers, skips):
            x = up(x)
            concat = tf.keras.layers.Concatenate()
            x = concat([x, skip])

        x = last_layer(x)

        self.unet = tf.keras.Model(inputs=inputs, outputs=x)

    def compile_unet(self, optimizer, loss, metrics):
        if not isinstance(metrics, list) and not isinstance(metrics, dict):
            print('Metrics needs to be given as a list or dictionary, setting to [accuracy]')
            metrics = metrics
        self.unet.compile(optimizer, loss, metrics)

    def train_model(self, ds, batch_size, epochs, displayCallback=False, validation_fraction = VALIDATION_FRACTION):
        # The model requires the input to have shape (iteration, height, width, channel),
        # so this is cacheing, and batching.

        if ds.storage == 'tf':
            ds.cache_and_batch_datset(batch_size)
        elif ds.storage =='numpy':
            ds.train['images'] = np.stack(ds.train['images'])
            ds.train['masks'] = np.stack(ds.train['masks'])

        # Validation data = data used to evaluate loss/other model metrics at the end of each epoch

        # TODO: We need to nail down what this means exactly
        # I think it means we train of the entire train dataset each epoch and validate on the entire validation
        # dataset at the end of each epoch when batch_size = 1

        self.validation_steps = ds.validation_size // batch_size # total number of steps (i.e. batches of samples) to draw
                                                                 # before stopping when performing validation at end of each epoch
        # if (ds.train_size * (1- validation_fraction)) % batch_size == 0:

        self.steps_per_epoch = (ds.train_size - ds.validation_size) // batch_size # total number of steps (i.e. batches of samples) before declaring one epoch
                                                           # finished and starting the next epoch
        # when batch_size is 2... trains on 40 samples per epoch (i.e. half of the training data) - accuracy went down but something to test?
        remainder = 0
        if (ds.train_size * (1- validation_fraction)) % batch_size != 0:
            remainder = int(ds.train_size * (1- validation_fraction)) % batch_size
            print('Remainder: {}'.format(remainder))


        if ds.storage == 'tf':
            for image, mask in ds.test.take(1):
                sample_image, sample_mask = image, mask
        elif ds.storage == 'numpy':
            index = random.randint(0, len(ds.test['images']) - 1)
            sample_image = ds.test['images'][index]
            sample_mask = ds.test['images'][index]

        # print('Length: {} \n Stepspepock: {} \n validationSteps: {} \n validationSplit: {}'.format(len(ds.train['images']),\
        #      self.steps_per_epoch,\
        #     self.validation_steps, validation_fraction))
        
        if ds.storage == 'tf':
            print('Steps Per Epoch: {}'.format(self.steps_per_epoch))
            if displayCallback:
                model_history = self.unet.fit(ds.train, epochs=epochs, shuffle = True,
                                            steps_per_epoch=self.steps_per_epoch,
                                            validation_steps=self.validation_steps,
                                              validation_data=ds.validation,
                                            # validation_split = validation_fraction,
                                            callbacks=[DisplayCallback(sample_image, sample_mask)])
            else:
                model_history = self.unet.fit(ds.train, epochs=epochs, shuffle= True,
                                            steps_per_epoch=self.steps_per_epoch,
                                            validation_steps=self.validation_steps,
                                              validation_data=ds.validation
                                            # validation_split = validation_fraction,
                                            )
        elif ds.storage == 'numpy':
            random_index = np.arange(len(ds.train['images']))
            np.random.shuffle(random_index)
            count = int(len(random_index) * validation_fraction)
            if displayCallback:
                model_history = self.unet.fit(x = ds.train['images'][count:], y = ds.train['masks'][count:], epochs=epochs, shuffle = True,
                                            # steps_per_epoch=self.steps_per_epoch,
                                            # validation_steps=1,
                                            batch_size = batch_size,
                                              validation_data=(ds.train['images'][:count], ds.train['masks'][:count]),
                                            # validation_split = validation_fraction,
                                            callbacks=[DisplayCallback(sample_image[np.newaxis, ...], sample_mask[np.newaxis, ...])])
            else:
                model_history = self.unet.fit(x = ds.train['images'][count:], y = ds.train['masks'][count:], epochs=epochs, shuffle= True,
                                            # steps_per_epoch=self.steps_per_epoch,
                                            # validation_steps=1,
                                            batch_size = batch_size,
                                             validation_data=(ds.train['images'][:count], ds.train['masks'][:count]),
                                            # validation_split = validation_fraction,
                                            )

        return model_history


    def evaluate_on_test_data(self, test_data, storage = 'tf'):
        if storage == 'tf':
            if len(test_data._flat_shapes[0]) < 4:
                test_data.batch(1)
            test_history = self.unet.evaluate(test_data)

        elif storage == 'numpy':
            test_data['images'] = np.stack(test_data['images'])
            test_data['masks'] = np.stack(test_data['masks'])
            test_history = self.unet.evaluate(test_data['images'], test_data['masks'])
        print("Loss for test data: {}, \nAccuracy on test data: {}".format(test_history[0], test_history[1]))
        return test_history

    def display_model_architecture(self):
        tf.keras.utils.plot_model(self.unet, show_shapes=True)
