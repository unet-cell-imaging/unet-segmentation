import matplotlib.pyplot as plt
import tensorflow as tf

from constants import * 
from dataset import Dataset
from network import UNet
from callback import DisplayCallback
from visualizations import display_images, create_mask



class Classifier:
    def __init__(self, dataset_size = DATASET_SIZE, 
    parent_directory = "cell_images", 
    update_status = UPDATE_STATUS ,
    seed_value = 5, 
    verbose = True, 
    test_size = TEST_FRACTION, 
    train_size = TRAIN_FRACTION, 
    validation_size = VALIDATION_FRACTION, 
    image_size = IMAGE_SIZE, 
    validation_test = VALIDATION_TEST, 
    epochs = EPOCHS, 
    optimizer = OPTIMIZER, 
    loss = LOSS,
    metrics = METRICS,
    batch_size = BATCH_SIZE, 
    last_layer_activation = 'softmax', 
    output_channels = OUTPUT_CHANNELS, 
    testing = False, 
    display_during_training = True):
        
        #Construct the parallel Dataset
        self.dataset = Dataset(dataset_size = dataset_size, 
        parent_directory = "cell_images", 
        update_status = update_status ,
        seed_value = 5, 
        verbose = True, 
        test_frac = test_size, 
        train_frac = train_size,
        validation_frac = validation_size, 
        k_folds = 10, 
        testing = testing, 
        image_size = image_size)
        
        # Set the number out output channels in the model for binary classification 
        # the number is 2
        self.output_channels = output_channels

        #Construct the Unet 
        self.model = UNet(
        unet_output_channels = self.output_channels,
        last_layer_activation = 'softmax',
        image_size = IMAGE_SIZE, 
        optimizer = 'adam',
        loss_function = 'sparse_categorical_crossentropy',
        metrics = ['accuracy'])

        #Num of Epochs 
        self.epochs = epochs
        # Optimizer, Default is adam
        self.optimizer = optimizer
        # Loss function 
        self.loss = loss

        self.metrics = metrics

        # Compile the model, still not trained
        self.compile_model(optimizer = self.optimizer, 
        loss = self.loss, metrics = metrics)

        # Is the Unet trained yet? 
        self.trained = False

        self.batch_size = batch_size

    

    # This function is for re-compiling the model if that's needed
    # within the experiment. 
    def compile_model(self, optimizer, loss, metrics):
        self.model.compile_unet(optimizer, loss, metrics)
        
    """
    For displaying the image and the mask, the data set retreives something from a generator()
    object, so it needs to loop through the object returned by take(). I haven't found a solution 
    for this yet. 
    """
    def display(self, iteration = None, dataset_type = 'training', show_prediction = False):
        iteration = 1 if iteration is None else iteration
        prediction = None
        if dataset_type == 'training':
            for image, mask in self.dataset.train.take(iteration):
                if show_prediction:
                    prediction = create_mask(self.model.unet.predict(image))
                    # print("Prediction Value: {}".format(prediction))
                display_images([image, mask, prediction])
        elif dataset_type == 'testing':
            for image, mask in self.dataset.test.take(iteration):
                display_images([image, mask])
        else:
            for image, mask in self.dataset.dataset.take(iteration):
                display_images([image, mask])


    # Area under the curve metric, in case we need it
    def AUC(y_true, y_pred):
        true_positives = tf.keras.backend.sum(tf.floor((y_true + y_pred) * 0.5))
        p = true_positives/tf.keras.backend.sum(y_pred + tf.keras.backend.epsilon())
        r = true_positives/tf.keras.backend.sum(y_true + tf.keras.backend.epsilon())
        return 2*((p * r)/(r + p))

    def run_experiment(self):
        self.show_predictions()

    def recompile(self):
        self.model.compile_unet(self.optimizer, self.loss, self.metrics)

    def train_model(self):
        # The model requires the input to have shape (iteration, height, width, channel), 
        # so this is cacheing, and batching. 
        self.dataset.cache_and_batch_datset(self.batch_size)

        # TODO: We need to nail down what this means exactly
        self.validation_steps = self.dataset.validation_size // self.batch_size
        self.steps_per_epoch = self.dataset.train_size // self.batch_size

        for image, mask in self.dataset.test.take(1):
            sample_image, sample_mask = image, mask

        # TODO: We might need 
        self.model_history = self.model.unet.fit(self.dataset.train, epochs = self.epochs,
        steps_per_epoch = self.steps_per_epoch, 
        validation_steps = self.validation_steps, 
        validation_data = self.dataset.validation, 
        callbacks = [DisplayCallback(sample_image, sample_mask)])



    def evaluate_on_test_data(self):
        self.test_history = self.model.evaluate(self.dataset.test)

        print("Loss for test data: {}, \nAccuracy on test data: {}".format(self.test_history[0], self.test_history[1]))





