import matplotlib
import matplotlib.pyplot as plt

from dataset import Dataset
from network import UNet
from visualizations import display_cost, display_test_results, display_images
from parsing import args

from tensorflow.keras.optimizers import Adam

from metrics import AUC


if args.verbose > 0:
    print('Running Unet Image Segmentation Experiment segmenting cell nuclei with ', 
    'prior training on synthetic cell image dataset. No training done on nuclei cell image dataset.')
# Set parameters
EPOCHS = 10

storage_type = args.storage_type

# Create dataset
# I'm only setting this to 100, there's no training done in this dataset. 
ds_nuclei = Dataset(dataset_size = 650, parent_directory = "nuclei_images", \
    image_types='nuclei', verbose=False, storage= storage_type)

ds_synthetic = Dataset(dataset_size = 1200, parent_directory = "cell_images",\
     image_types='synthetic', verbose = False, storage = storage_type)

if args.verbose > 0:
    print("Dataset Loaded")

# Preprocess data (resize, normalize, and flip)
ds_nuclei.preprocess_dataset() #resizes to 128
ds_synthetic.preprocess_dataset()


# display_images([ds.nuclei.dataset['images'][3], ds.nuclei.dataset['masks'][3]])

# Display sample images
# for sample_image, sample_mask in ds_nuclei.dataset.take(1):
#     display_images([sample_image, sample_mask])

# Well Use most of this data for testing, but if I set training to zero, it'll break
ds_nuclei.split(test_frac=0.9, train_frac=0.1, validation_frac=0.25) # validation_frac = subset of training data

ds_synthetic.split(test_frac= .2 , train_frac=0.8, validation_frac=0.25) # validation_frac = subset of training data

# Create model
model = UNet(unet_output_channels = 2, last_layer_activation = 'softmax', image_size = 128)

my_optimizer = Adam(learning_rate= 0.00075)


# Compile model
model.compile_unet(optimizer = my_optimizer,loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])

# Display images with untrained model. This doesn't really matter, since this experiment doesn't really 
# train on any of the nuclei images. 
display_test_results(ds_nuclei.test, model, take_num=1, storage= ds_nuclei.storage)
print("Ipython: {}".format(args.ipython_display))

# Train model -> we want to train on the synthetic cell images. 
model_history = model.train_model(ds_synthetic, batch_size=16, epochs=EPOCHS, displayCallback=args.ipython_display)

# Display cost
display_cost(model_history, epochs=EPOCHS)

# Display test result images _. we're testing on the nuclei data
display_test_results(ds_nuclei.test, model, take_num=1,
storage = storage_type)

# Evaluate accuracy
# ds_nuclei.cache_and_batch_datset(1)
test_history = model.evaluate_on_test_data(ds_nuclei.test, 
storage=storage_type)

