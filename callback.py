import tensorflow as tf
from IPython.display import clear_output


from visualizations import display_images, create_mask



class DisplayCallback(tf.keras.callbacks.Callback):

    def __init__(self, image, mask):
        tf.keras.callbacks.Callback.__init__(self)
        self.image = image
        self.mask = mask

    def on_epoch_end(self, epoch, logs = None):
        clear_output(wait = True) # We'll only need this if running 
        # in jupyter notebook
        self.show_predictions()
        print ('\nSample Prediction after epoch {}\n'.format(epoch+1))


# Shows the predictions for the masks, used inside the display function 
    def show_predictions(self, num = 1):
        predicted_mask = self.model.predict(self.image)
        display_images([self.image[0], self.mask[0], create_mask(predicted_mask)])


    