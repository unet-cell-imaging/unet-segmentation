import matplotlib
import matplotlib.pyplot as plt

from dataset import Dataset
from network import UNet
from visualizations import display_cost, display_test_results, display_images
from parsing import args



# Set parameters
EPOCHS = 20


# Create dataset
ds = Dataset(dataset_size = 10, parent_directory = "cell_images", image_types='synthetic', storage= 'numpy')

# Preprocess data (resize, normalize, and flip)
ds.preprocess_dataset() #resizes to 128

# Display sample images
# for sample_image, sample_mask in ds.dataset.take(1):
#     display_images([sample_image, sample_mask])
display_images([ds.dataset['images'][3], ds.dataset['masks'][3]])
