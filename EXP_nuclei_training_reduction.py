import matplotlib
import matplotlib.pyplot as plt

from dataset import Dataset
from network import UNet
from visualizations import display_cost, display_test_results, display_images, display_accuracy
from parsing import args

from tensorflow.keras.optimizers import Adam

if args.verbose > 0:
    print('Running Unet Image Segmentation Experiment with optimized parameters', 
    'prior training on synthetic cell image dataset. Small amount of training done on the nuclei test set.')
# Set parameters
EPOCHS = 10

storage_type = 'numpy'
# Create dataset
ds_nuclei = Dataset(dataset_size = 650, parent_directory = "nuclei_images", \
    image_types='nuclei', verbose=False, storage= storage_type)

ds_synthetic = Dataset(dataset_size = 1200, parent_directory = "cell_images",\
     image_types='synthetic', verbose = False, storage = storage_type)

if args.verbose > 0:
    print("Dataset Loaded")

# Preprocess data (resize, normalize, and flip)
ds_nuclei.preprocess_dataset() #resizes to 128
ds_synthetic.preprocess_dataset()


# display_images([ds.nuclei.dataset['images'][3], ds.nuclei.dataset['masks'][3]])

# Display sample images
# for sample_image, sample_mask in ds_nuclei.dataset.take(1):
#     display_images([sample_image, sample_mask])


training_size = args.training_size
training_fraction = training_size / ds_nuclei.dataset_size
testing_fraction = 1 - training_fraction
print('Starting Dataset initialization \nTesting Fraction : {}'.format(testing_fraction))

# Split data
ds_nuclei.split(test_frac=testing_fraction, train_frac=training_fraction, validation_frac=0.25) # validation_frac = subset of training data
ds_synthetic.split(test_frac=0.2, train_frac=0.8, validation_frac=0.25) # validation_frac = subset of training data

# Create model
model = UNet(unet_output_channels = 2, last_layer_activation = 'softmax', image_size = 128)

my_optimizer = Adam(learning_rate= 0.00075)

# Compile model
model.compile_unet(optimizer = my_optimizer,loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])

# Display images with untrained model. This doesn't really matter, since this experiment doesn't really 
# train on any of the nuclei images. 
display_test_results(ds_nuclei.test, model, take_num=1, storage= ds_nuclei.storage)
print("Ipython: {}".format(args.ipython_display))


# Train model
model_history_synthetic = model.train_model(ds_synthetic, batch_size=16, epochs=EPOCHS, displayCallback=args.ipython_display)

print('Results after training on the synthetic set without any training on the nucleu set.')
# Display cost
display_cost(model_history_synthetic, epochs=EPOCHS)

# Display test result images
display_test_results(ds_nuclei.test, model, take_num=1,
storage = storage_type)

display_accuracy(model_history_synthetic, epochs= EPOCHS)

# Evaluate accuracy
# ds_nuclei.cache_and_batch_datset(1)
print('Performance on Synthetic Set:')
test_history = model.evaluate_on_test_data(ds_synthetic.test, 
storage= storage_type)

print('Performance on Nuclei Set:')
test_history = model.evaluate_on_test_data(ds_nuclei.test, 
storage= storage_type)

display_test_results(ds_nuclei.test, model, take_num=2, storage= ds_nuclei.storage)

display_test_results(ds_nuclei.test, model, take_num=3, storage= ds_nuclei.storage)

display_test_results(ds_nuclei.test, model, take_num=4, storage= ds_nuclei.storage)


# Train model
model_history_nuclei = model.train_model(ds_nuclei, batch_size=16, epochs=EPOCHS, displayCallback=args.ipython_display)

print('Results after training on a small set of the Nuclei Data')
# Display cost
display_cost(model_history_nuclei, epochs=EPOCHS)

# Display test result images
display_test_results(ds_nuclei.test, model, take_num=1,
storage = storage_type)

display_accuracy(model_history_nuclei, epochs= EPOCHS)

# Evaluate accuracy
# ds_nuclei.cache_and_batch_datset(1)
print('Performance on Synthetic Set after nuclei training:')
test_history = model.evaluate_on_test_data(ds_synthetic.test, 
storage= storage_type)

print('Performance on Nuclei Set after nuclei training:')
test_history = model.evaluate_on_test_data(ds_nuclei.test, 
storage= storage_type)

display_test_results(ds_nuclei.test, model, take_num=2, storage= ds_nuclei.storage)

display_test_results(ds_nuclei.test, model, take_num=3, storage= ds_nuclei.storage)

display_test_results(ds_nuclei.test, model, take_num=4, storage= ds_nuclei.storage)






