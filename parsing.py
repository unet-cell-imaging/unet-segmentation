import argparse


parser = argparse.ArgumentParser()

# Should there be a display? For use in a Jupyter Notebook. The default is true
parser.add_argument('--ipython_display', dest = 'ipython_display', action='store_true')

# Should Display be repressed? Defalt is false. I know this seems redundant with the 
# above argument, but I had to do this to make it work. 
parser.add_argument('--no-ipython_display', dest = 'ipython_display', action='store_false')
parser.set_defaults(ipython_display=False)

# What is the dataset size?
parser.add_argument('--data_size', type =  int)

parser.add_argument('--storage', dest = 'storage_type' , type = str)
parser.set_defaults(storage_type = 'numpy')

parser.add_argument('--training_size', dest= 'training_size', type = int)
parser.set_defaults(training_size = 481)



# Should Status Updates be disaplyed when running the script? 
# Default is no. 
parser.add_argument('--verbose', dest = 'verbose', type = int)
parser.set_defaults(verbose = 0)

args = parser.parse_args()

