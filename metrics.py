import tensorflow_core as tf

# Area under the curve metric, in case we need it
def AUC(y_true, y_pred):
    """
    For evaluating the area under the curve. It's thought this might be useful for class imbalance,
    but in preliminary testing we found this was not very good. 
    """
    true_positives = tf.keras.backend.sum(tf.floor((y_true + y_pred) * 0.5))
    p = true_positives/(tf.keras.backend.sum(y_pred) + tf.keras.backend.epsilon())
    r = true_positives/(tf.keras.backend.sum(y_true) + tf.keras.backend.epsilon())
    return 2*((p * r)/(r + p))