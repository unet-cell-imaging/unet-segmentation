"""
Tests for the dataset class. This should make sure the import, splitting, etc works. 
"""
# TODO: Testing the dataset splitting 

import pytest

from dataset import Dataset
import visualizations

@pytest.fixture
def default_dataset():
    return Dataset(dataset_size=10, storage='numpy', verbose= True)

# @pytest.fixture
# def default_dataset_no_import():
#     return Dataset(testing=True)

def test_instance_build(default_dataset):
    
    assert isinstance(default_dataset, Dataset)

def test_dataset_splits(default_dataset):
    assert default_dataset.train_size < default_dataset.dataset_size, "Please use train_frac in the range of [0,1]"
    assert default_dataset.validation_size < default_dataset.dataset_size, "Please use test_frac in the range of [0,1]"
    assert default_dataset.test_size < default_dataset.dataset_size, "Please use validation_frac in the range of [0,1]"

    assert default_dataset.train_size + \
        default_dataset.test_size \
             == default_dataset.dataset_size, \
            "All fractions must sum to one. The following Sizes. Training: {} \
                Testing: {} Validation: {}".format(default_dataset.train_size, \
                    default_dataset.test_size, default_dataset.validation_size)

def test_flipping_and_resizing(default_dataset):
    default_dataset.preprocess_dataset()
    default_dataset.initial_split()
    print(default_dataset.train._flat_shapes[0][0])
    print(default_dataset.train._flat_shapes[0][1])
    assert default_dataset.train._flat_shapes[0][0] == 128, default_dataset.train._flat_shapes[0]


def test_directory_read_in():
    sample_dataset = Dataset(dataset_size = 10, image_types='nuclei', parent_directory= 'nuclei_images', storage= 'tf')

def test_numpy(default_dataset):
    # print(default_dataset.dataset['images'].shape)
    default_dataset.preprocess_dataset()
    
    visualizations.display_images([default_dataset.dataset['images'][3], default_dataset.dataset['masks'][3]])
    default_dataset.split(test_frac=0.2, train_frac=0.8, validation_frac=0)

    print(default_dataset.train['images'][0].shape)
    train_batch = default_dataset.batch_dataset(default_dataset.train['images'], 32)
    print(train_batch.shape)


