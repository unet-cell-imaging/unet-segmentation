"""
Testing the unet compiling and functions

"""
import pytest

from network import UNet

@pytest.fixture()
def unet_sample():
    return UNet(unet_output_channels = 3)

def test_unet_build(unet_sample):
    assert isinstance(unet_sample, UNet)

