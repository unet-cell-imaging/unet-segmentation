"""
This is the testing script for the overarching classifier itself. 
"""
import pytest 
import tensorflow as tf
import matplotlib.pyplot as plt

from classifier import Classifier

@pytest.fixture()
def sample_classifier():
    return Classifier(display_during_training=False)

# @pytest.fixture()
# def sample_dataset_no_import():
#     return Classifier(testing = True)

def test_classifier_build(sample_classifier):
    assert isinstance(sample_classifier, Classifier)

def test_display_function(sample_classifier):
    sample_classifier.dataset.initial_split()
    assert isinstance(sample_classifier.dataset.train, tf.data.Dataset)
    sample_classifier.display()
    plt.show()

def test_caching(sample_classifier):
    # sample_classifier.dataset.initial_split()
    sample_classifier.dataset.cache_and_batch_datset(1)
    assert len(sample_classifier.dataset.train._flat_shapes[0]) == 4, "Size of Dataset: {}".format(sample_classifier.dataset.train._flat_shapes[0])

def test_training(sample_classifier):
    print(sample_classifier.dataset.train._flat_shapes[0])
    sample_classifier.train_model()



