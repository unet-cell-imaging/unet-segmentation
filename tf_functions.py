"""
There's specific functions called from tensorflow that get called from the 
map() function. For now I'm putting them here in their own seperate space, 
and incorporate them into their respective class if I have time. 
"""

"""
For resizing and flipping the training instances. 
"""
import numpy as np
import tensorflow as tf

from constants import IMAGE_SIZE

@tf.function
def resize_and_flip(image, mask):
    
    # Normalize the dataset to [0,1]
    input_image = tf.image.resize(image, (IMAGE_SIZE, IMAGE_SIZE))
    input_mask = tf.image.resize(mask, (IMAGE_SIZE, IMAGE_SIZE))
    if tf.random.uniform(()) > 0.5:
        input_image = tf.image.flip_left_right(input_image)
        input_mask = tf.image.flip_left_right(input_mask)
    # if tf.random.uniform(()) < 1:
    input_image = tf.multiply(255 - input_image, -1)
    print('Resized and flipped')

    input_image, input_mask = normalize(input_image, input_mask)

    return input_image, input_mask


def normalize(input_image, input_mask):
    input_image = tf.cast(input_image, tf.float32) / 255.0
    # Mask is binary
    # input_mask = int(input_mask / 255)
    return input_image, input_mask


def resize_and_flip_nontf(image, mask, invert = True):
    
    # Normalize the dataset to [0,1]
    input_image = tf.image.resize(image, (IMAGE_SIZE, IMAGE_SIZE))
    input_mask = tf.image.resize(mask, (IMAGE_SIZE, IMAGE_SIZE))
    if tf.random.uniform(()) > 0.5:
        input_image = tf.image.flip_left_right(input_image)
        input_mask = tf.image.flip_left_right(input_mask)
    if tf.random.uniform(()) < 0.25 and invert:
        input_image = (np.ones(input_image.shape) * 255) - input_image
    # print('Resized and flipped')

    input_image, input_mask = normalize(input_image, input_mask)

    return input_image, input_mask