
import matplotlib
import matplotlib.pyplot as plt

from dataset import Dataset
from network import UNet
from visualizations import display_cost, display_test_results, display_images
from parsing import args



# Set parameters
EPOCHS = 20


# Create dataset
ds = Dataset(dataset_size=args.data_size, parent_directory = "cell_images")

# Preprocess data (resize, normalize, and flip)
ds.preprocess_dataset() #resizes to 128

# Display sample images
for sample_image, sample_mask in ds.dataset.take(1):
    display_images([sample_image, sample_mask])
    
# Split data
ds.split(test_frac=0.2, train_frac=0.8, validation_frac=0.25) # validation_frac = subset of training data

# Create model
model = UNet(unet_output_channels = 2, last_layer_activation = 'softmax', image_size = 128)

# Compile model
model.compile_unet(optimizer = 'adam',loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])

# Display images with untrained model
display_test_results(ds.train, model, take_num=1)
print("Ipython: {}".format(args.ipython_display))
# Train model
model_history = model.train_model(ds, batch_size=1, epochs=EPOCHS, displayCallback=args.ipython_display)

# Display cost
display_cost(model_history, epochs=EPOCHS)

# Display test result images
display_test_results(ds.test, model, take_num=1)

# Evaluate accuracy
test_history = model.evaluate_on_test_data(ds.test)


#TODO: figure out what's going on with the training (batching/validation) - test different batch sizes/cross-validation techniques
#TODO: test different activation functions?