import matplotlib

import matplotlib.pyplot as plt

from classifier import Classifier
sample_classifier = Classifier()

sample_classifier.dataset.initial_split()
sample_classifier.display()
plt.scatter([1,2], [1,2])

plt.show()