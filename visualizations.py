"""
Separate visualization file for seeing the masks. 
I think this might be easier for the Callback() class
that we call when training the model. 
"""
import random 
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np


def create_mask(predicted_mask):
        predicted_mask = tf.argmax(predicted_mask, axis = -1)
        predicted_mask = predicted_mask[..., tf.newaxis]
        return predicted_mask[0]


'''
For displaying image and mask together, the hope is that it'll work for a 
'''
def display_images(display_list):
    plt.figure(figsize=(15, 15))
    title = ['Input Image', 'True Mask', 'Predicted Mask']
    list_size = sum(x is not None for x in display_list) 
    for i in range(list_size):
        if display_list[i] is not None:
            plt.subplot(1, list_size, i+1)
            plt.title(title[i])
            if not np.ndarray:
                numpy_image = display_list[i].numpy()
            else:
                numpy_image = display_list[i]
            if len(numpy_image.shape) < 4:
                # plt.imshow(tf.keras.preprocessing.image.array_to_img(display_list[i]))
                plt.imshow(numpy_image[:,:,0], cmap = 'gray')
            else:
                # plt.imshow(tf.keras.preprocessing.image.array_to_img(display_list[i][0,:,:,:]))
                # numpy_image = display_list[i].numpy()
                plt.imshow(numpy_image[0,:,:,0], cmap = 'gray')
        plt.axis('off')
    plt.show()

def display_cost(model_history, epochs):
    plt.figure()
    plt.plot(range(epochs), model_history.history['loss'], 'r', label='Training loss')
    plt.plot(range(epochs), model_history.history['val_loss'], 'bo', label='Validation loss')
    plt.title('Training and Validation Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss Value')
    plt.ylim([0, 1])
    plt.legend()
    plt.show()

# Build the predicted mask
def create_mask(pred_mask):
    pred_mask = tf.argmax(pred_mask, axis=-1)
    pred_mask = pred_mask[..., tf.newaxis]
    return pred_mask[0]

# Show the original image, the mask, and the predicted mask.
def show_predictions(model, dataset, num=1, storage = 'tf'):
    if dataset:
        if storage == 'tf':
            for image, mask in dataset.take(num):
                pred_mask = model.unet.predict(image)
                display_images([image[0], mask[0], create_mask(pred_mask)])
        elif storage == 'numpy':
            # index = random.randint(0, len(dataset['images']))
            image = dataset['images'][num]
            mask = dataset['masks'][num]
            pred_mask = model.unet.predict(image[np.newaxis,...] )
            display_images([image, mask, create_mask(pred_mask)])


def display_test_results(ds, model, take_num=3, storage = 'tf'):
    if storage == "tf":
        if len(ds._flat_shapes[0]) < 4:
            ds = ds.batch(1)
    elif storage =='numpy':
            ds['images'] = np.stack(ds['images'])
            ds['masks'] = np.stack(ds['masks'])

    show_predictions(model, ds, take_num, storage)


def display_accuracy(model_history, epochs, title = 'Model Accuracy', saveFig = False, file_name = ''):
    plt.figure()
    plt.plot(model_history.history['accuracy'])
    plt.plot(model_history.history['val_accuracy'])
    plt.legend(['training', 'validation'], loc='upper left')
    plt.title(title)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    if saveFig:
        plt.savefig(file_name)
    else:
        pass
        # plt.show()

