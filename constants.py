"""
All the default constants needed for the model. 

"""

DATASET_SIZE = 100
UPDATE_STATUS = 10
FILE_DIRECTORY = ""

PARENT_DIRECTORY = ""
MASK_DIRECTORY = ""
IMAGE_DIRECTORY = ""

IMAGE_SIZE = 128


OUTPUT_CHANNELS = 2

TEST_FRACTION = 0.3
TRAIN_FRACTION = 0.7

VALIDATION_FRACTION = 0.2

VALIDATION_TEST = '3fold'

OPTIMIZER = 'adam'

LOSS = 'sparse_categorical_crossentropy'

METRICS = ['accuracy']

EPOCHS = 5

BATCH_SIZE = 1
