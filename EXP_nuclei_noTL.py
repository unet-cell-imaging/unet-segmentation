import matplotlib
import matplotlib.pyplot as plt

from dataset import Dataset
from network import UNet
from visualizations import display_cost, display_test_results, display_images
from parsing import args

from tensorflow.keras.optimizers import Adam

if args.verbose > 0:
    print('Running Unet Image Segmentation Experiment ssing nuclei cell images without any', 
    'prior training on an synthetic celll image dataset.')
# Set parameters
EPOCHS = 10

storage_type = args.storage_type
print('Storage: {}'.format(storage_type))
# Create dataset
ds = Dataset(dataset_size = 650, parent_directory = "nuclei_images", image_types='nuclei', verbose= False, storage = storage_type)

if args.verbose > 0:
    print("Dataset Loaded")

# Preprocess data (resize, normalize, and flip)
ds.preprocess_dataset() #resizes to 128

# Display sample images
# for sample_image, sample_mask in ds.dataset.take(1):
#     display_images([sample_image, sample_mask])

# Split data
ds.split(test_frac=0.2, train_frac=0.8, validation_frac=.2) # validation_frac = subset of training data

# Create model
model = UNet(unet_output_channels = 2, last_layer_activation = 'softmax', image_size = 128)

my_optimizer = Adam(learning_rate= 0.00075)


# Compile model
model.compile_unet(optimizer = my_optimizer,loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])

# Display images with untrained model
display_test_results(ds.train, model, take_num=1, storage = ds.storage)
print("Ipython: {}".format(args.ipython_display))




# Train model
model_history = model.train_model(ds, batch_size=16, epochs=EPOCHS, displayCallback=args.ipython_display)
# Display cost
display_cost(model_history, epochs=EPOCHS)

# Display test result images -> it's only the nuclei dataset, so we can just pass test
display_test_results(ds.test, model, take_num=1, storage = storage_type)

# Evaluate accuracy
test_history = model.evaluate_on_test_data(ds.test, storage = storage_type)
