'''
This is the dataset class. It holds the images and all the functions 
for changing the altering the dataset (splitting, porcessing). 
Called within the Classifier Class. 
'''
from os import walk

import numpy as np
import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE
from PIL import Image
import random

from constants import *
from tf_functions import resize_and_flip, normalize, resize_and_flip_nontf

class Dataset:
    """
    Dataset class to hold the image files in a Tensorflow data set. 

    Parameters: 
    images_types: The type of images to load in the Datset. The options are 'synthetic' for all synthetic 
        cell images, and 'nuclei' for all images that have the real cell numclei images with masks. 
    """
    def __init__(self, dataset_size = DATASET_SIZE, parent_directory = "cell_images", update_status = UPDATE_STATUS,
                 seed_value = 5, verbose = True, k_folds = 10, testing = False, image_size = IMAGE_SIZE, 
                 image_types = 'synthetic', storage = 'tf'):
        self.images = {}
        self.masks = {}
        self.raw_images = {}
        self.dataset_size = dataset_size
        self.parent_directory = parent_directory
        self.update_status = update_status
        self.seed_value = seed_value
        self.verbose = verbose
        self.k_folds = k_folds
        self.image_size = image_size
        self.storage = storage
        # self.establish_dataset_splits(train_frac, test_frac, validation_frac)
        if not testing:
            self.dataset = None
            self.read_in_slices(image_types)
        del self.images
        del self.masks
        del self.raw_images

    
    def assemble_filenames_nuclei(self):
        """
        Tranverse the directory. This only works if you have a folder in the PARENT_DIRECTORY, 
        titled 'images' and one titled 'masks'. 
        Appends the filenames together in a dictionary with 'images' and 'masks'. 

        Dictionary contains:
        Image class (image or mask):
            parent directory: original directory stated in PARENT_DIRECTORY
            files: list of all the files in that fiolder, should all be TIF
        """
        self.raw_images['final'] = {}
        self.raw_images['test'] = {}
        self.raw_images['train'] = {}
        for dir_path, dirname, file_names in walk(str(self.parent_directory)):
        # Prevents .ipynbcheckpoint directories from getting picked up
            if len(file_names) > 0:
                image_class = dir_path.split("/")[-1]
                [parent_directory, split, image_file, _ ] = dir_path.split("/")
                try:
                    self.raw_images[split][image_file][image_class] = file_names
                    self.raw_images[split][image_file]['parent_directory'] = "/".join(dir_path.split("/")[:-1])
                except KeyError:
                    self.raw_images[split][image_file] = {}
                    self.raw_images[split][image_file][image_class] = file_names
                    self.raw_images[split][image_file]['parent_directory'] = "/".join(dir_path.split("/")[:-1])
        # print(len(self.raw_images['train'].items()))
        # print('files: {}'.format(random.choice(list(self.raw_images['train'].items()))))
        # print('Parent Directory: {}'.format(self.parent_directory))
    
    def assemble_filenames_synthetic(self):
        """
        Tranverse the directory. This only works if you have a folder in the PARENT_DIRECTORY, 
        titled 'images' and one titled 'masks'. 
        Appends the filenames together in a dictionary with 'images' and 'masks'. 

        Dictionary contains:
        Image class (image or mask):
            parent directory: original directory stated in PARENT_DIRECTORY
            files: list of all the files in that fiolder, should all be TIF
        """
        for dir_path, dirname, file_names  in walk(str(self.parent_directory)):
        # Prevents .ipynbcheckpoint directories from getting picked up
            if len(file_names) > 0:
                img_class = dir_path.split("/")[-1]
                self.raw_images[img_class] = {}
                self.raw_images[img_class]['parent_directory'] = "/".join(dir_path.split("/")[:-1])
                self.raw_images[img_class]['files'] = file_names
                # print("/".join(dir_path.split("/")[:-1]))

    def batch_dataset(self, df, batch_size):
        if len(df) % batch_size == 0:
            iterations = len(df) // batch_size
        else:
            iterations = (len(df) // batch_size) + 1

        # temp_df = np.zeros((iterations, batch_size, df[0].shape[0], df[0].shape[1], df[0].shape[2]))
        temp_df = [] 
        temp_df = np.zeros((iterations, df[0].shape[0], df[0].shape[1], df[0].shape[2]))
        for i in range(iterations):
            temp_batch = np.zeros((batch_size, df[0].shape[0], df[0].shape[1], df[0].shape[2]))
            temp_batch = None
            temp_df.append()
        return temp_df




    def binarize(self, image):
        """
        To conver mask images to 1 or 0 when read into the dataset. The binarizes the mask images 
        before doing any normalization so it doesn't have to use a tensor. 

        image: the MASK image as a 2D numpy array

        returns: any Values that are > 1 as ones, and 0 other wise
        """

        image[image > 0] = 1
        return image

    def cache_and_batch_datset(self, batch_size):
        """
        For caching and batching the dataset. The image sets need to be reshaped to a 
        four-dimensional object, where the new dimension is the batch number. 
        The function here tests to make sure the objects are not resized yet, and 
        caches and batches them. 
        TODO: Detemine what the caching does. 
        """
        if self.storage == 'tf':
            if len(self.train._flat_shapes[0]) < 4:
                self.train = self.train.cache().batch(batch_size).repeat()
                self.train = self.train.prefetch(buffer_size = tf.data.experimental.AUTOTUNE)
            if len(self.validation._flat_shapes[0]) < 4:
                self.validation = self.validation.batch(batch_size)
                self.test = self.test.batch(batch_size)
        elif self.storage == 'numpy':
            self.train = 0

    # TODO: I need to figure out how tensorflow is working with this validation splits
    def cross_validation_split(self, method):
        assert self.test_size + self.validation_size + self.train_size == 1
        if method == "k_fold":
            pass
        elif method == "leave_one_out":
            pass
        elif method == "hold_out":
            self.train  = self.dataset.take(int(self.dataset_size * (self.train_size + self.validation_size)))
            self.validation = self.train.skip(int(self.train_size))
        
    # TODO: This needs to be updated with the right dataset splits. 
    def split(self,test_frac = TEST_FRACTION, train_frac = TRAIN_FRACTION, validation_frac = VALIDATION_FRACTION):
        if self.storage == 'tf':
            # establish sizes
            self.train_size = int(self.dataset_size * train_frac)
            self.validation_size = int(self.train_size * validation_frac)
            self.test_size = int(self.dataset_size * test_frac)

            # print(self.train_size, self.validation_size, self.test_size)

            self.dataset = self.dataset.shuffle(self.dataset_size)

            self.test = self.dataset.take(self.test_size)
            self.train = self.dataset.skip(self.test_size)
            self.validation = self.train.skip(self.train_size - self.validation_size)
        elif self.storage == 'numpy':
            train_size = int(len(self.dataset['images']) * train_frac)
            test_size = len(self.dataset['images']) - train_size
            random_indices = np.arange(len(self.dataset['images']))
            np.random.shuffle(random_indices)
            self.validation_size = int(train_size * validation_frac)
            self.train_size = train_size
            train_indices = [r for r in random_indices[0:train_size]]
            test_indices = [r for r in random_indices[train_size:]]
            self.train = {}
            self.test = {}
            self.train['images'] = [self.dataset['images'][idx] for idx in train_indices]
            self.train['masks'] = [self.dataset['masks'][idx] for idx in train_indices]

            self.test['images'] = [self.dataset['images'][idx] for idx in test_indices]
            self.test['masks'] = [self.dataset['masks'][idx] for idx in test_indices]

    """
    Calls the function for flipping/resizing. I wanted a function here that I wouldn't need to
    give any arguments for.
    """
    def preprocess_dataset(self):
        if self.storage =='tf':
            self.dataset = self.dataset.map(resize_and_flip, num_parallel_calls = tf.data.experimental.AUTOTUNE)
        else:
            for idx, _ in enumerate(self.dataset['images']):
                # print(idx)
                self.dataset['images'][idx], self.dataset['masks'][idx] = resize_and_flip_nontf(self.dataset['images'][idx], self.dataset['masks'][idx])

    def read_in_slices(self, image_types):
        """
        Reading in the images into the Dataset class. Makes a call to either read_in_slices_synthetic()
        or read_in_sclices_nuclei depending on the image_type given. 

        :image_type: can either be 'synthetic' or 'nuclei'. The default is synthetic. 
        """
        if self.verbose:
            print('Image Type: {}'.format(image_types))
        if image_types == 'synthetic':                  
            self.assemble_filenames_synthetic()
            self.read_in_slices_synthetic()
        elif image_types == 'nuclei':
            self.assemble_filenames_nuclei()
            self.read_in_slices_nuclei()


        # Now that the file names are in a list, read in the image files using the file names
    def read_in_slices_nuclei(self, subset = 'train'):
        """
        For reading in images from the nuclei databace from Kaggle. The images in this 
        dataset are broken up a little differently than in the synthetic database. 
        It goes: 
        nuclei_images
        |-train
        |   |-image_specific_file
        |        |-images
        |        |-masks
        |            |-seperate image file for each nuclei
        |-test
        |   |-image_specific_file
        |       |-images
        |-final
        |   |-image_specific_file
        |       |-images


        """
        image_slices = []
        mask_slices = []

        keys = list(self.raw_images[subset].keys())

        random_index = np.arange(len(keys))

        np.random.shuffle(random_index)

        # Go through all the mask files, mask is a subset of images, 
        # so we only need to read in the images that have a corresponding mask.  
        for i, image in enumerate(self.raw_images[subset]):
            try:
                # Read in image files, This assumes there only one image in the images folder. 
                image_temp = Image.open(str(self.raw_images[subset][keys[random_index[i]]]['parent_directory'] +\
                                                    "/images/" + self.raw_images[subset][keys[random_index[i]]]['images'][0]))
                # image_slice = np.array(image_temp.resize((self.image_size, self.image_size), Image.ANTIALIAS))
                # NOTE: Added this in for RGBA files, right now lets only deal with RGB
                image_slice = np.array(image_temp.resize((self.image_size, self.image_size), Image.ANTIALIAS))[:, :, 0:3]

                # This dataset keeps a seperate mask image for every nuclei im the original image, so we can just add them 
                # together and we should get one image slice, and then we can use the rest of the existing machinery
                # print(self.raw_images[subset][keys[random_index[i]]]['parent_directory'])
                for m, mask in enumerate(self.raw_images[subset][keys[random_index[i]]]['masks']):
                    mask_temp = Image.open(str(self.raw_images[subset][keys[random_index[i]]]['parent_directory'] +\
                                                    "/masks/" + self.raw_images[subset][keys[random_index[i]]]['masks'][m]))
                    mask_temp = np.array(mask_temp.resize((self.image_size, self.image_size), Image.ANTIALIAS))
                    # print(np.unique(mask_temp))
                    mask_temp = mask_temp[..., np.newaxis]
                    if m < 1:
                        mask_slice = mask_temp #// 255
                    else:
                        mask_slice = mask_slice + mask_temp 

                # Some of this dataset has bad masks, skip these and append decrement the dataset size by one. 
                if np.array_equal(image_slice, mask_slice):
                    self.dataset_size -= 1
                    print('Found Similar')
                    continue
                image_slices.append(image_slice)
                mask_slice = self.binarize(mask_slice)
                mask_slices.append(mask_slice)
                
                if i%self.update_status == 0 and self.verbose:
                    print(i , "Files Read")
                # Limiting our import 
                if i > self.dataset_size:
                    break
            except OSError:
                print("Non-TIFF File Found")
                continue
        

        if self.storage == 'tf':
        # Final Parallel Dataset, everything else is deleted
            if self.verbose: print("Importing Tensor Slices")
            self.dataset = tf.data.Dataset.from_tensor_slices((image_slices, mask_slices))
        else:
            self.dataset = {}
            if self.verbose: print('Converting to dictionary')
            self.dataset['images'] = image_slices
            self.dataset['masks'] = mask_slices
        
        if self.verbose: print("Data Import Complete")

        del image_slices
        del mask_slices


    # Now that the file names are in a list, read in the image files using the file names
    def read_in_slices_synthetic(self):
        image_slices = []
        mask_slices = []

        random_index = np.arange(len(self.raw_images['masks']['files']))
        # np.random.seed(self.seed_value)
        np.random.shuffle(random_index)

        # Go through all the mask files, mask is a subset of images, 
        # so we only need to read in the images that have a corresponding mask.  
        for i, image in enumerate(self.raw_images['masks']['files']):
            try:
                # Read in image files, and convert to a numpy array
                image_slice = np.array(Image.open(str(self.raw_images['images']['parent_directory'] +\
                                                    "/images/" +\
                                                    self.raw_images['masks']['files'][random_index[i -1]])))
                mask_slice = np.array(Image.open(str(self.raw_images['images']['parent_directory'] +\
                                                    "/masks/" +\
                                                    self.raw_images['masks']['files'][random_index[i -1]])))
                # Resize the numpy arrays to an 'RGB' set, showing the same value repeated 3 times 
                image_slices.append(np.repeat(image_slice[..., np.newaxis], 3, -1))
                
                # Masks are not repeated, they need to remain just a classification for each pixel in range [0,1]
                mask_slices.append(self.binarize(mask_slice)[..., np.newaxis])
                
                
                if i%self.update_status == 0 and self.verbose:
                    print(i , "Files Read")
                # Limiting our import 
                if i > self.dataset_size:
                    break
            except OSError:
                print("Non-TIFF File Found")
                continue
            
        if self.storage == 'tf':
        # Final Parallel Dataset, everything else is deleted
            if self.verbose: print("Importing Tensor Slices")
            self.dataset = tf.data.Dataset.from_tensor_slices((image_slices, mask_slices))
        else:
            self.dataset = {}
            if self.verbose: print('Converting to dictionary')
            self.dataset['images'] = image_slices
            self.dataset['masks'] = mask_slices

        del image_slices
        del mask_slices




if __name__ == '__main__':
    Dataset()
